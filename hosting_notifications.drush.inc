<?php

/**
 * Implementation of drush_hook_post_hosting_task().
 */
function drush_hosting_notifications_post_hosting_task() {
  if (module_exists('hosting_notifications')) {
    $task = &drush_get_context('HOSTING_TASK');

    $event = array(
      'module' => 'hosting_notifications',
      'type' => 'hosting_task',
      'action' => 'succeed',
    );
    notifications_event($event, array('task' => $task));
  }
}

/**
 * Implementation of drush_hook_pre_hosting_task().
 *
 * This is needed so that Drush runs our correspsonding rollback:
 * drush_hosting_notifications_pre_hosting_task_rollback().
 */
function drush_hosting_notifications_pre_hosting_task() {

}

/**
 * Implementation of drush_hook_pre_hosting_task_rollback().
 *
 * We catch the error from drush_hosting_task() here.
 */
function drush_hosting_notifications_pre_hosting_task_rollback() {
  if (module_exists('hosting_notifications')) {
    $task = &drush_get_context('HOSTING_TASK');

    $event = array(
      'module' => 'hosting_notifications',
      'type' => 'hosting_task',
      'action' => 'fail',
    );
    notifications_event($event, array('task' => $task));
    // hook_drush_exit() won't be called if we're doing a rollback.
    hosting_notifications_drush_exit();
  }
}

/**
 * Implementation of hook_drush_exit().
 *
 * If notifications are sent to send immediately, they won't be when using
 * drush because hook_exit() is not invoked, but hook_drush_exit is.
 */
function hosting_notifications_drush_exit() {
  if (function_exists('notifications_exit')) {
    notifications_exit();
  }
}
