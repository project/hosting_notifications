<?php

/**
* Implementation of hook_notifications().
*/
function hosting_notifications_notifications($op, &$arg0, $arg1 = NULL, $arg2 = NULL) {
  switch ($op) {
    case 'event classes':
      return array('hosting_task' => t('Hosting task'));

    case 'event actions':
      return array(
        'succeed' => t('Task succeeded'),
        'fail' => t('Task failed'),
      );

    case 'subscription fields':
      $fields = array();
      $fields['tasktype'] = array(
        'name' => t('Task type'),
        'field' => 'tasktype',
        'type' => 'string',
        'options callback' => 'hosting_notifications_task_type_options',
      );
      $fields['taskreference'] = array(
        'name' => t('Task reference'),
        'field' => 'taskreference',
        'type' => 'string',
        'options callback' => 'hosting_notifications_task_reference_options',
      );
      return $fields;

    case 'object types':
      $types = array();

      $types['task'] = array(
        'name' => t('Hosting Task'),
        'key_field' => 'vid',
        'load callback' => 'hosting_notifications_task_load',
        'access callback' => 'hosting_notifications_task_user_access',
      );

      return $types;
    case 'event types':  // define email templates @ admin/messaging/notifications/events
      $types = array();
        $types['hosting_task-succeed'] = array( // type-action
          'type' => 'hosting_task',
          'action' => 'succeed',
          'description' => t('Hosting task succeeded'),
          'template' => 'hosting-notifications-task-succeed',
        );
        $types['hosting_task-fail'] = array( // type-action
          'type' => 'hosting_task',
          'action' => 'succeed',
          'description' => t('Hosting task succeeded'),
          'template' => 'hosting-notifications-task-fail',
        );
        return $types;
  }
}

/**
* Implementation of hook_notifications_event().
*/
function hosting_notifications_notifications_event($op, $event, $account = NULL) {
  switch ($op) {
    case 'query':
      $query[]['fields'] = array('tasktype' => $event->objects['task']->task_type);
      if (!empty($event->objects['task']->rid) && ($ref = node_load($event->objects['task']->rid))) {
        $query[]['fields'] = array('taskreference' => $ref->type);
      }
      return $query;
  }
}

/**
* Implementation of hook_notifications_templates().
*/
function hosting_notifications_notifications_templates($op, $type = 'all', $language = NULL) {
  switch ($op) {
    case 'info':
      $info = array();
      if ($type == 'all' || $type == 'hosting-notifications-task-succeed') {
        $info['hosting-notifications-task-succeed'] = array(
          'module' => 'hosting_notifications',
          'name' => t('Notifications for Hosting task success'),
          'help' => t('The subject and main body will be provided by the event itself'),
          'description' => t('For notifications resulting from Hosting task executing successfully.'),
          'fallback' => 'notifications-event',
        );
      }
      if ($type == 'all' || $type == 'hosting-notifications-task-fail') {
        $info['hosting-notifications-task-fail'] = array(
          'module' => 'hosting_notifications',
          'name' => t('Notifications for Hosting task failure'),
          'help' => t('The subject and main body will be provided by the event itself'),
          'description' => t('For notifications resulting from Hosting task executing unsuccessfully.'),
          'fallback' => 'notifications-event',
        );
      }
      return $info;
    case 'parts':
      switch ($type) {
        case 'hosting-notifications-task-succeed':
          return array(
            'subject' => t('Subject'),
            'main' => t('Content'),
            'digest' => t('Digest Line'),
          );
        case 'hosting-notifications-task-fail':
          return array(
            'subject' => t('Subject'),
            'main' => t('Content'),
            'digest' => t('Digest Line'),
          );
      }
      break;
    case 'defaults':
      switch ($type) {
        case 'hosting-notifications-task-succeed':
          return array(
            'subject' => t('Task success: [title]', array(), $language->language),
            'main' => array(
              t('The task:', array(), $language->language),
              '[title]',
              t('has succeeded.', array(), $language->language),
              '',
              t('You can view more details here:', array(), $language->language),
              '[node-url]'
            ),
            'digest' => array(
              t('Task success: [title]', array(), $language->language),
              t('View detail [node-url]', array(), $language->language),
            ),
          );
        case 'hosting-notifications-task-fail':
          return array(
            'subject' => t('Task failure: [title]', array(), $language->language),
            'main' => array(
              t('The task:', array(), $language->language),
              '[title]',
              t('has failed to execute successfully.', array(), $language->language),
              '',
              t('You can view more details here:', array(), $language->language),
              '[node-url]'
            ),
            'digest' => array(
              t('Task failure: [title]', array(), $language->language),
              t('View detail [node-url]', array(), $language->language),
            ),
          );
      }
      break;
    case 'tokens':
      $tokens = array();
      switch ($type) {
        case 'hosting-notifications-task-succeed':
        case 'hosting-notifications-task-fail':
          $tokens[] = 'task';
          break;
      }
      return $tokens;
  }
}

/**
 * Implementation of hook_token_list()
 */
function hosting_notifications_token_list($type = 'all') {
  $tokens = array();
  if ($type == 'task' || $type == 'all') {
    $node_tokens = token_get_list('node');
    $tokens['task'] = $node_tokens['node'];
  }
  return $tokens;
}

/**
 * Implementation of hook_token_values()
 */
function hosting_notifications_token_values($type, $object = NULL, $options = array()) {
  switch ($type) {
    case 'task':
      $tmp_tokens = module_invoke_all('token_values', 'node', $object, $options);
      return $tmp_tokens;
  }
}

/**
 * Callback for all task types that can be subscribed to.
 */
function hosting_notifications_task_type_options() {
  $options = array();
  foreach (array_keys(hosting_notifications_task_reference_options()) as $key) {
    $tasks = hosting_available_tasks($key);
    foreach ($tasks as $name => $task) {
      $options[$name] = $task['title'];
    }
  }
  if (!empty($options)) {
    asort($options);
  }
  return $options;
}

/**
 * Callback for all task references that can be subscribed to.
 */
function hosting_notifications_task_reference_options() {
  $options = array(
    'server' => t('Server'),
    'platform' => t('Platform'),
    'site' => t('Site'),
  );
  return $options;
}

/**
 * Load a task by vid.
 */
function hosting_notifications_task_load($vid) {
  return node_load(array('vid' => $vid));
}

/**
 * Check user access to task.
 */
function hosting_notifications_task_user_access($node, $account) {
  return node_access('view', $node, $account);
}
