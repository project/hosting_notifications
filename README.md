Hosting Notifications
=====================

This is a simple module for the Aegir project that integrates with the
Notifications framework. This allows you to receive notifications about task
execution in various formats, Email, Twitter, iPhone etc.

Requirements
------------

This module requires the custom subscriptions module, which is part of the 4.x
version of the Notifications framework.

Installation
------------

Install and enable like any other Drupal module.

Configuration
-------------

You'll need to set up some subscriptions that users can subscribe to, do so
within the Custom notifications admin pages: `admin/messaging/customsubs/new`.
Choose 'Hosting task' for the Event type. You will then be able to choose from
various fields for the subscription, you choose at least one from:

* Action - Filter by whether the task succeeded or failed.
* Task type - Filter by the type of task, so for example users could subscribe
to just 'migrate' tasks.
* Task reference - Filter by the object that the task is being performed on,
'Server', 'Platform' or 'Site'.

Once you've add the subscriptions, users can subscribe from the Notifications
tab in their user pages.
